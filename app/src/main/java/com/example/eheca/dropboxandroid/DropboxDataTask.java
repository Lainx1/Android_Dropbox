package com.example.eheca.dropboxandroid;

import android.os.AsyncTask;
import android.util.Log;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderBuilder;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.sharing.CreateSharedLinkWithSettingsErrorException;
import com.dropbox.core.v2.sharing.GetFileMetadataErrorException;
import com.dropbox.core.v2.sharing.ListSharedLinksResult;
import com.dropbox.core.v2.sharing.RequestedVisibility;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dropbox.core.v2.sharing.SharedLinkSettings;

import java.util.List;

/**
 * Created by eheca on 10/07/2018.
 */

public class DropboxDataTask extends AsyncTask<Void, String, Void> {
    DbxClientV2 client;
    private String path;
    private AsyncTastkInterfaces tastkInterfaces;

    public interface AsyncTastkInterfaces{
        void pre_execute();
        void progress_update(String... values);
        void post_execute();
    }

    public DropboxDataTask(String path, DbxClientV2 client, AsyncTastkInterfaces tastkInterfaces){
        this.client = client;
        this.path = path;
        this.tastkInterfaces = tastkInterfaces;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        //getDropboxData();
        createLinks();
        getSharedLinks();
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        tastkInterfaces.pre_execute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        tastkInterfaces.post_execute();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        tastkInterfaces.progress_update(values);
    }
    private void getSharedLinks(){
        try{
            //ListFolderBuilder builder = client.files().listFolderBuilder(path);
            //ListFolderResult result = builder.withRecursive(true).start();
            List<SharedLinkMetadata> listMetaData = client.sharing().listSharedLinks().getLinks();
            for (SharedLinkMetadata metadata: listMetaData){
                System.out.println(metadata.getName() + " " + metadata.getUrl());
                publishProgress(metadata.getName());
            }
        }catch (DbxException ex){

        }
    }
    private void createLinks(){
        try{
            ListFolderBuilder builder = client.files().listFolderBuilder(path);
            ListFolderResult result = builder.withRecursive(true).start();
            do {
                for (Metadata metadata: result.getEntries())
                    if (metadata instanceof FileMetadata)
                        try{
                            client.sharing().createSharedLinkWithSettings(metadata.getPathLower()
                                    , SharedLinkSettings.newBuilder().withRequestedVisibility(RequestedVisibility.PUBLIC).build());
                        }catch (CreateSharedLinkWithSettingsErrorException ex){
                            Log.e(getClass().getSimpleName(), "Link already exists");
                        }
                result = client.files().listFolderContinue(result.getCursor());
            }while (result.getHasMore());
        }catch (DbxException ex){
            ex.printStackTrace();
        }


    }
    private void getDropboxData(){
        try{
            ListFolderBuilder builder = client.files().listFolderBuilder(path);
            ListFolderResult result = builder.withRecursive(true).start();
            while (true){
                if (result != null){
                    for (Metadata metadata : result.getEntries()){
                        if (metadata instanceof FileMetadata){
                            publishProgress(metadata.getName());
                            try{
                                ListSharedLinksResult linksResult = client.sharing().listSharedLinksBuilder()
                                        .withPath(metadata.getPathLower()).withDirectOnly(true).start();
                                String url = null;
                                if (linksResult.getLinks().size() == 0){
                                    Log.i(getClass().getSimpleName(), "Creating Dropbox URL");
                                    try {
                                        SharedLinkMetadata slm = client.sharing().createSharedLinkWithSettings(metadata.getPathLower()
                                                , SharedLinkSettings.newBuilder().withRequestedVisibility(RequestedVisibility.PUBLIC).build());
                                        url = slm.getUrl();
                                    }catch (CreateSharedLinkWithSettingsErrorException e){
                                        Log.e(getClass().getSimpleName(), e.getMessage());
                                    }
                                }
                                if (url == null){
                                    url = client.sharing().listSharedLinksBuilder().withPath(metadata.getPathLower())
                                            .start().getLinks().get(0).getUrl();
                                }
                                System.out.println(metadata.getName() +  " " + url);
                            }catch (GetFileMetadataErrorException ex){
                                Log.e(getClass().getSimpleName(), ex.getMessage());
                            }
                            //}
                        }
                    }
                    if (!result.getHasMore()){
                        break;
                    }
                    try{
                        result = client.files().listFolderContinue(result.getCursor());
                    }catch (DbxException ex){
                        System.out.println("Error; "+ex.getMessage());
                    }
                }
            }
        }catch (DbxException ex){
            Log.e(this.getClass().getSimpleName(), ex.getMessage());
        }
    }
}
