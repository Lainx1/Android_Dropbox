package com.example.eheca.dropboxandroid;

import android.os.AsyncTask;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;

/**
 * Created by eheca on 26/06/2018.
 */

public class UserAccountTask extends AsyncTask<Void, Void, FullAccount> {
    private DbxClientV2 dbxClient;
    private TaskDelegate delegate;
    private Exception error;

    public interface TaskDelegate{
        void onAccountRecived(FullAccount account);
        void error(Exception error);
    }

    UserAccountTask(DbxClientV2 dbxClient, TaskDelegate delegate){
        this.dbxClient = dbxClient;
        this.delegate = delegate;
    }

    @Override
    protected FullAccount doInBackground(Void... voids) {
        try {
            return dbxClient.users().getCurrentAccount();
        }catch (DbxException e){
            e.printStackTrace();
            error = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(FullAccount account) {
        super.onPostExecute(account);
        if (account != null && error == null){
            //User Account received successfully
            delegate.onAccountRecived(account);
        }else{
            // Something went wrong
            delegate.error(error);
        }
    }
}
