package com.example.eheca.dropboxandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuth;
import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.auth.DbxUserAuthRequests;
import com.dropbox.core.v2.filerequests.DbxUserFileRequestsRequests;
import com.dropbox.core.v2.filerequests.FileRequest;
import com.dropbox.core.v2.filerequests.ListFileRequestsResult;
import com.dropbox.core.v2.files.DbxUserFilesRequests;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.GetMetadataBuilder;
import com.dropbox.core.v2.files.ListFolderBuilder;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.MediaMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.sharing.CreateSharedLinkWithSettingsErrorException;
import com.dropbox.core.v2.sharing.GetFileMetadataErrorException;
import com.dropbox.core.v2.sharing.ListFilesResult;
import com.dropbox.core.v2.sharing.ListSharedLinksResult;
import com.dropbox.core.v2.sharing.RequestedVisibility;
import com.dropbox.core.v2.sharing.SharedLinkErrorException;
import com.dropbox.core.v2.sharing.SharedLinkMetadata;
import com.dropbox.core.v2.sharing.SharedLinkSettings;
import com.dropbox.core.v2.users.FullAccount;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private String ACCESS_TOKEN = "";
    private TextView displayNameTextView, statusTextView, threadTextView;
    private ListView listItems;
    private List<String> names;
    private ArrayAdapter<String> adapter;
    private String path = "";
    private DbxClientV2 dropboxClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

        displayNameTextView = findViewById(R.id.displayNameTextView);
        statusTextView = findViewById(R.id.statusTextView);
        threadTextView = findViewById(R.id.threadTextView);
        listItems = findViewById(R.id.listItems);

        names = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        listItems.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void login(View view){
        Auth.startOAuth2Authentication(this, getString(R.string.dropbox_app_key));
    }
    public void signOut(View view){
        if (dropboxClient == null)return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dropboxClient.auth().tokenRevoke();
                    dropboxClient = null;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            displayNameTextView.setText("name");
                            statusTextView.setText("Status");
                            threadTextView.setText("Status");
                            names.clear();
                            adapter.clear();
                            adapter.notifyDataSetChanged();
                        }
                    });
                } catch (DbxException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    @Override
    protected void onResume() {
        super.onResume();

        getAccessToken();
    }

    private void getAccessToken() {
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.sharedpreferences_dropbox_file), MODE_PRIVATE);
        ACCESS_TOKEN = Auth.getOAuth2Token() != null ? Auth.getOAuth2Token() : sharedPreferences.getString(getString(R.string.sharedpreferences_access_token), null);
        if (ACCESS_TOKEN != null){

            sharedPreferences.edit().putString(getString(R.string.sharedpreferences_access_token), ACCESS_TOKEN).apply();

            DbxRequestConfig config = new DbxRequestConfig(MainActivity.class.getSimpleName());
            dropboxClient = new DbxClientV2(config, ACCESS_TOKEN);

            getUserAccount();
        }

    }
    protected void getUserAccount(){
        if (dropboxClient == null)return;

        new UserAccountTask(dropboxClient, new UserAccountTask.TaskDelegate() {
            @Override
            public void onAccountRecived(FullAccount account) {
                Log.d("User", account.getEmail());
                Log.d("Email", account.getName().getDisplayName());
                Log.d("Account Type", account.getAccountType().name());
                updateUi(account);
                new DropboxDataTask(path, dropboxClient, new DropboxDataTask.AsyncTastkInterfaces() {
                    @Override
                    public void pre_execute() {
                        statusTextView.setText("Ready to scan");
                        threadTextView.setText("Waiting");
                    }

                    @Override
                    public void progress_update(String... values) {
                        names.add(values[0]);
                        statusTextView.setText("Scanning " + values[0]);
                        threadTextView.setText("Working");
                    }

                    @Override
                    public void post_execute() {
                        adapter.addAll(names);
                        adapter.notifyDataSetChanged();
                        statusTextView.setText("Complete");
                        threadTextView.setText("Finish");
                    }
                }).execute();
            }
            @Override
            public void error(Exception error) {
                Log.d("User", "Error receiving account details.");
                updateUi(error);
            }
        }).execute();
    }
    private void updateUi(FullAccount fullAccount){
        displayNameTextView.setText("Account: "+fullAccount.getName().getDisplayName());
    }
    private void updateUi(Exception error){
        displayNameTextView.setText("Error: "+error.getMessage());
    }
}